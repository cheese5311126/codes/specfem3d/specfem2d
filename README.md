# SPECFEM2D

[![GitLab Release (latest by SemVer)](https://img.shields.io/github/v/release/SPECFEM/specfem2d)](https://github.com/SPECFEM/specfem2d/releases)
[![DOI](https://zenodo.org/badge/14293189.svg)](https://zenodo.org/doi/10.5281/zenodo.7434515)


SPECFEM2D allows users to perform 2D and 2.5D (i.e., axisymmetric) simulations
of acoustic, elastic, viscoelastic, and poroelastic seismic wave propagation.
The package can also be used for full waveform imaging (FWI) or adjoint tomography.

## Documentation

For further information, please visit the SPECFEM2D 
[User Guide](https://specfem2d.readthedocs.io/en/latest/).

## Developers

SPECFEM2D was founded by Dimitri Komatitsch and Jeroen Tromp, and is now being developed by a large, collaborative, and inclusive community. A complete list of authors can be found at
https://specfem2d.readthedocs.io/en/latest/authors/

## License

GNU GENERAL PUBLIC LICENSE

Version 3, 29 June 2007

Copyright © 2007 Free Software Foundation, Inc. http://fsf.org/

Everyone is permitted to copy and distribute verbatim copies of this

license document, but changing it is not allowed.

## Copyright

Main historical authors: Dimitri Komatitsch and Jeroen Tromp

CNRS, France and Princeton University, USA $\copyright$ October 2017

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation (see Appendix [cha:License]).

Please note that by contributing to this code, the developer understands and agrees that this project and contribution are public and fall under the open source license mentioned above.

